package hu.chemaxon.fibonacci.controller;

import hu.chemaxon.fibonacci.calculator.FibonacciCalculator;
import hu.chemaxon.fibonacci.domain.FibonacciResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class FibonacciController {

    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @GetMapping("/fibonacci-calculator")
    public String fibonacciCalculator(Model model) {
        FibonacciResult fibonacciResult = new FibonacciResult();
        model.addAttribute("fibonacciResult", fibonacciResult);
        return "/fibonacci-calculator";
    }

    @PostMapping("/fibonacci-calculator")
    public String calculate(@Valid @ModelAttribute("fibonacciResult") FibonacciResult fibonacciResult, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            fibonacciResult.setResult(FibonacciCalculator.calculate(fibonacciResult.getNumber()));
        }
        return "/fibonacci-calculator";
    }
}
