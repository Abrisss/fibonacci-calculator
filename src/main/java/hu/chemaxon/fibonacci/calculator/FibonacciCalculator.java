package hu.chemaxon.fibonacci.calculator;

import java.util.ArrayList;
import java.util.List;

public class FibonacciCalculator {

    public static List<Long> calculate(int number) {
        Long numberOne = 0L;
        Long numberTwo = 1L;
        Long numberThree;
        List<Long> result = new ArrayList<>();

        if (number == 0) {
            result.add(numberOne);
        } else {
            result.add(numberOne);
            result.add(numberTwo);
            for (int i = 1; i < number; i++) {
                numberThree = numberOne + numberTwo;
                numberOne = numberTwo;
                numberTwo = numberThree;
                result.add(numberThree);
            }
        }
        return result;
    }
}
