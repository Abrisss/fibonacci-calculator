package hu.chemaxon.fibonacci.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class FibonacciResult {
    //Maybe here I could have used BigInteger instead of Long. I chose Long for simplicity.
    private List<Long> result;

    @NotNull
    @Min(0)
    @Max(92)
    private int number;

    public List<Long> getResult() {
        return result;
    }

    public void setResult(List<Long> result) {
        this.result = result;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
