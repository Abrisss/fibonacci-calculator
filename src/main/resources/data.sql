-- Later on, passwords need to be saved encrypted.
INSERT INTO users (username, password, enabled) VALUES ('user', 'user', TRUE);

INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_ADMIN');