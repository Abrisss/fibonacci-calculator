DROP TABLE IF EXISTS users;

CREATE TABLE users (
  username VARCHAR(256),
  password VARCHAR(256),
  enabled  BOOLEAN
);

DROP TABLE IF EXISTS authorities;

CREATE TABLE authorities (
  username  VARCHAR(256),
  authority VARCHAR(256)
);