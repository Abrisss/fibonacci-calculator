package hu.chemaxon.fibonacci.calculator;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FibonacciCalculatorTest {

    @Test
    public void testWithZero() {
        //GIVEN
        int number = 0;

        //WHEN
        List<Long> result = FibonacciCalculator.calculate(number);

        //THEN
        assertThat(result)
                .hasSize(1)
                .extracting(aLong -> aLong)
                .containsExactly(
                        0L
                );
    }

    @Test
    public void testWithOne() {
        //GIVEN
        int number = 1;

        //WHEN
        List<Long> result = FibonacciCalculator.calculate(number);

        //THEN
        assertThat(result)
                .hasSize(2)
                .extracting(aLong -> aLong)
                .containsExactly(
                        0L,
                        1L
                );
    }

    @Test
    public void test() {
        //GIVEN
        int number = 5;

        //WHEN
        List<Long> result = FibonacciCalculator.calculate(number);

        //THEN
        assertThat(result)
                .hasSize(6)
                .extracting(aLong -> aLong)
                .containsExactly(
                        0L,
                        1L,
                        1L,
                        2L,
                        3L,
                        5L
                );
    }

    @Test
    public void testWithMaxValue() {
        //GIVEN
        int number = 92;

        //WHEN
        List<Long> result = FibonacciCalculator.calculate(number);

        //THEN
        assertThat(result)
                .hasSize(93)
                .extracting(aLong -> aLong)
                .contains(
                        0L,
                        1L,
                        2111485077978050L,
                        7540113804746346429L
                );
    }
}
